#!/usr/bin/python

from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf
from subprocess import call

class LinuxRouter( Node ):
    "A Node with IP forwarding enabled."

    def config( self, **params ):
        super( LinuxRouter, self).config( **params )
        # Enable forwarding on the router
        self.cmd( 'sysctl net.ipv4.ip_forward=1' )

    def terminate( self ):
        self.cmd( 'sysctl net.ipv4.ip_forward=0' )
        super( LinuxRouter, self ).terminate()

def myNetwork():

    net = Mininet( topo=None,
                   build=False,
                   ipBase='10.0.0.0/8',
                   autoStaticArp=True)

    info( '*** Adding controller\n' )
    c1=net.addController(name='c1',
                      controller=RemoteController,
                      ip='192.168.50.2',
                      protocol='tcp',
                      port=6633)

    info( '*** Add switches\n')
    s2 = net.addSwitch('s2', cls=OVSKernelSwitch)
    r1 = net.addHost('r1', cls=LinuxRouter, ip='172.16.80.254/24')
    s1 = net.addSwitch('s1', cls=OVSKernelSwitch)

    info( '*** Add hosts\n')
    lan2 = net.addHost('lan2', cls=Host, ip='192.168.80.2/24', defaultRoute='via 192.168.80.254')
    app_server = net.addHost('app_server', cls=Host, ip='172.16.80.1/24', defaultRoute='via 172.16.80.254')
    web_server = net.addHost('web_server', cls=Host, ip='172.16.80.2/24', defaultRoute='via 172.16.80.254')
    lan1 = net.addHost('lan1', cls=Host, ip='192.168.80.1/24', defaultRoute='via 192.168.80.254')

    info( '*** Add links\n')
    net.addLink(s1, app_server)
    net.addLink(s2, lan2)
    net.addLink(s1, web_server)
    net.addLink(s2, lan1)
    net.addLink(r1, s1)
    net.addLink(r1, s2)

    info( '*** Starting network\n')
    net.build()
    info( '*** Starting controllers\n')
    for controller in net.controllers:
        controller.start()

    info( '*** Starting switches\n')
    net.get('s2').start([c1])
    net.get('s1').start([c1])

    info( '*** Post configure switches and hosts\n')
    s2.cmd('ifconfig s2 10.0.0.2/24')
    s1.cmd('ifconfig s1 10.0.0.1/24')

    r1.cmd('ifconfig r1-eth0 172.16.80.254/24')
    r1.cmd('ifconfig r1-eth1 192.168.80.254/24')

    CLI(net)
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    myNetwork()

