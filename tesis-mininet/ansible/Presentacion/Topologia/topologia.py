#!/usr/bin/python

from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf
from subprocess import call

def myNetwork():

    net = Mininet( topo=None,
                   build=False,
                   ipBase='10.0.0.0/8',
                   autoStaticArp=True)

    info( '*** Adding controller\n' )
    c0=net.addController(name='c0',
                      controller=RemoteController,
                      ip='192.168.50.2',
                      protocol='tcp',
                      port=6633)

    info( '*** Add switches\n')
    s2 = net.addSwitch('s2', cls=OVSKernelSwitch, dpid='0000000000000002')
    s10 = net.addSwitch('s10', cls=OVSKernelSwitch, dpid='000000000000000a')
    s1 = net.addSwitch('s1', cls=OVSKernelSwitch, dpid='0000000000000001')
    s11 = net.addSwitch('s11', cls=OVSKernelSwitch, dpid='000000000000000b')

    info( '*** Add hosts\n')
    User3 = net.addHost('User3', cls=Host, ip='10.0.0.202', defaultRoute=None)
    Boss = net.addHost('Boss', cls=Host, ip='10.0.0.2', defaultRoute=None)
    User2 = net.addHost('User2', cls=Host, ip='10.0.0.201', defaultRoute=None)
    Hacker = net.addHost('Hacker', cls=Host, ip='10.0.0.200', defaultRoute=None)
    AppServer1 = net.addHost('AppServer1', cls=Host, ip='10.1.0.1', defaultRoute=None)
    User1 = net.addHost('User1', cls=Host, ip='10.0.0.1', defaultRoute=None)

    info( '*** Add links\n')
    net.addLink(User1, s1)
    net.addLink(Boss, s1)
    s1s10 = {'bw':10}
    net.addLink(s1, s10, cls=TCLink , **s1s10)
    s1s11 = {'bw':1000}
    net.addLink(s1, s11, cls=TCLink , **s1s11)
    s10s2 = {'bw':1000}
    net.addLink(s10, s2, cls=TCLink , **s10s2)
    s11s2 = {'bw':1000}
    net.addLink(s11, s2, cls=TCLink , **s11s2)
    net.addLink(s2, AppServer1)
    net.addLink(s11, Hacker)
    net.addLink(s11, User2)
    net.addLink(s11, User3)

    info( '*** Starting network\n')
    net.build()
    info( '*** Starting controllers\n')
    for controller in net.controllers:
        controller.start()

    info( '*** Starting switches\n')
    net.get('s2').start([c0])
    net.get('s10').start([c0])
    net.get('s1').start([c0])
    net.get('s11').start([c0])

    info( '*** Post configure switches and hosts\n')

    CLI(net)
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    myNetwork()

