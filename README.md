Vagrant Base Image:

	Sistema Operativo: Ubuntu 16.04 (32 bits)
	Usuarios:
        root (tesis)
        tesis (tesis)
        vagrant (vagrant)


Pasos Configuración:
    
    1.- Clonar https://chelobarreto@gitlab.com/chelobarreto/tesis-laboratorio.git a la carpeta $HOME/Vagrant
        git clone https://chelobarreto@gitlab.com/chelobarreto/tesis-laboratorio.git $HOME/Vagrant
    2.- Descargar la imagen base desde https://www.dropbox.com/s/vaeym26vr8m0pg0/package.box?dl=0 a la carpeta $HOME/Vagrant/Base
        wget https://www.dropbox.com/s/vaeym26vr8m0pg0/package.box?dl=0 $HOME/Vagrant/Base/package.box
    3.- Instalar vagrant y virtualbox.
        sudo apt-get install vagrant virtualbox
    4.- Agregar el box base de vagrant
        vagrant box add $HOME/Vagrant/Base/package.box --name base-ubuntu-16-04-32

Pasos de Ejecución:
    
    1.- Controlador OpenFlow:
        a.- Ingresar a la carpeta $HOME/Vagrant/tesis-laboratorio/tesis-controller
        b.- Ejecutar: vagrant up
        cd $HOME/Vagrant/tesis-laboratorio/tesis-controller
        vagrant up
        c.- En caso de errores ejecutar: vagrant provision
        d.- Ingresar a la VM
        vagrant ssh
        e.- Ejecutar el controlador frenetic
        /etc/init.d/frenetic start
        f.- Ejecutar la aplicación backend
        python /home/vagrant/tesis/apps/main.py
        g.- Ejecutar la aplicacion frontend
        cd /home/vagrant/tesis-frontend/frontend
        gulp serve
    3.- Mininet:
        a.- Ingresar a la carpeta $HOME/Vagrant/tesis-laboratorio/tesis-mininet
        b.- Ejecutar: vagrant up
        cd $HOME/Vagrant/tesis-laboratorio/tesis-mininet
        vagrant up
        c.- En caso de errores ejecutar: vagrant provision
        d.- Ingresar a la VM
        vagrant ssh
        e.- sudo mn --topo tree,3,2 --mac --controller remote,ip=192.168.50.2
    4.- Acceder al frontend:
        http://ip_maquina_corre_vms:3000